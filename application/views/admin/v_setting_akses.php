<div class="card">
    <div class="header bg-green">
        <h2><?=$title?></h2>        
    </div> 
    <div class="body">
        <form id="form" enctype="multipart/form-data">
            <div class="row clearfix">
                <div class="col-md-6">                    
                    <label for="kategori_id">Pilih Akses</label>
                    <div class="form-group">
                        <div class="form-line">
                            <select class="form-control show-tick" data-live-search="true" name="id_role" id="id_role">
                                <option value="" disabled>Silahkan Pilih Akses</option>
                                <?php
                                    foreach ($dataRole as $role) {
                                        $selected=($id_role==$role->id_role) ? "selected":"" ;
                                        echo"<option ".$selected." value='".$role->id_role."'>".$role->nama_role."</option>";
                                    }
                                ?>
                            </select>   
                        </div>
                    </div>                    
                </div>                
            </div>
            <div class="row clearfix">
                <?php foreach ($dataCategoryPage as $categoryPage) {
                    
                    $category_id = $categoryPage->category_id;
                                $this->db->order_by('page_sort', 'asc'); 
                    $q_page =   $this->db->get_where('rs_page', array('category_id' => $category_id,'page_parent_id' => '0'));
                    $list='';
                    $jmlParent=0;
                    foreach ($q_page->result() as $page)
                    {
                        $checkPage = $this->db->get_where("rs_user_role",array("id_role"=>$id_role,"page_id"=>$page->page_id));
                        $checked = ($checkPage->num_rows() > 0) ? "checked":"";
                        $list.='<input type="checkbox" id="check-skiing'.$page->page_id.'" value="'.$page->page_id.'" name="page_id[]" '.$checked.' class="all_category'.$category_id.' filled-in chk-col-indigo">
                                <label for="check-skiing'.$page->page_id.'">'.$page->page_label.'</label></br>';
                                                                                                    
                        $page_id= $page->page_id;
                                    $this->db->order_by('page_sort', 'asc');
                        $q_parent = $this->db->get_where('rs_page', array('page_parent_id' => $page_id));
                        $jmlParent=$q_parent->num_rows();

                        if($jmlParent > 0){
                            foreach ($q_parent->result() as $parent){   
                                $q_parent = $this->db->get_where("rs_user_role",array("id_role"=>$id_role,"page_id"=>$parent->page_id));
                                $checked = ($q_parent->num_rows() > 0) ? "checked":"";
                                                                
                                $list.='
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="checkbox" id="check-skiing_parent'.$parent->page_id.'" value="'.$parent->page_id.'" name="page_id[]" '.$checked.' class="all_category'.$category_id.' filled-in chk-col-pink">
                                        <label for="check-skiing_parent'.$parent->page_id.'">'.$parent->page_label.'</label> </br>';
                                
                                
                            }                            
                        }
                        
                    }
                    ?>

                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="header bg-blue-grey">
                                <h2>
                                    <?=$categoryPage->category_title?>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons"><?=$categoryPage->category_icon?></i>
                                        </a>
                                    </li>
                                </ul> 
                            </div>
                            <div class="body"><?=$list?></div>
                        </div>
                    </div>

                <?php } ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <a id="save" href="javascript:void(0);" class="pull-right bg-black btn waves-effect btn-lg">Simpan</a>
                </div>                
            </div>
        </form>
    </div>
</div>

<script>
    $(document).on("change","#id_role",function(e){
        e.preventDefault();
        var id_role = $(this).val();
        document.location="<?php echo site_url()?>/admin/C_Setting/index/"+id_role;
    });
    
    $(document).on("click","#save",function(e){
        e.preventDefault();
        updateAccessUser();
    });
    

    function updateAccessUser() {
        var dataPost = $("form").serialize();
        var id_role = $("#id_role").val();
        //console.log(dataPost);

        swal({
            title: "Apakah kamu yakin ?",
            text: "Kamu akan memperbarui data ini",
            type: "info",
            showCancelButton: true,
            cancelButtonColor: "#F44336",
            confirmButtonColor: "#4CAF50",
            confirmButtonText: "Yes",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function (isConfirm) {
            if (!isConfirm) return;
            $.ajax({
                url: "<?php echo site_url('admin/C_Setting/updateAccessUser')?>",
                type: "POST",
                data: dataPost,
                success: function (data) {
                    var obj = jQuery.parseJSON(data)
                    //console.log(data);
                    var kode =obj.metaData.kode;
                    var pesan =obj.metaData.message;
                    if(kode=="200"){
                        swal("Berhasil",pesan, "success");                        
                        document.location="<?php echo site_url()?>/admin/C_Setting/index/"+id_role;
                    }else{
                        swal("Simpan Gagal",pesan, "error");
                    }
                    
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error koneksi !", "silahkan coba lagi "+thrownError, "error");
                }
            });
        });
    }
</script>
