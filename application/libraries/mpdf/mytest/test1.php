<?php
include '../mpdf.php';

$mpdf = new mPDF();
$mpdf->WriteHTML(file_get_contents("../../../../../bootstrap3/dist/css/bootstrap.min.css"),1);

$html='<table class="table table-bordered table-condensed table-striped">
	<thead>
		<tr>
			<th>Heading 1</th>
			<th>Heading 2</th>
			<th>Heading 3</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>Content 1</td>
			<td>Content 2</td>
			<td>Content 3</td>
		</tr>
	</tbody>
</table>';

$mpdf->WriteHTML($html);
$mpdf->Output();
exit();
