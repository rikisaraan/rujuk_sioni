<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

  	public function __construct(){ 
      	parent::__construct();     
		  $this->load->model('M_Karyawan','karyawan');
  	}    

  	public function index(){
      	$this->load->view('v_form_login');   
  	}

  	public function checkLogin(){
		$response = $this->karyawan->checkLogin();
		echo $response;
	}

	public function actChangePass(){
		$response = $this->karyawan->actChangePass();
		echo $response;
	}

	public function dashboard(){
		///print_r($this->session->userdata());exit;
		$data=array(
	            'body'=>'v_main',
	            'title'=>'Dashboard', 
	            'navbar'=>'admin',
				'index'=>'Dashboard',
				'id_role'=>$this->session->userdata("id_role"),
    	        'controller'=>'main',
    	        'act'=>'dashboard',
	        );  
		  $this->load->view('template_default',$data);
	}


	public function logout(){
			$response = $this->karyawan->logout();
			echo $response;
	}

	public function autoLogout(){
			$this->session->sess_destroy();
			redirect('main');
	}
	
	public function changePass(){
    	$data = array(
				'title' => 'Ubah Kata Sandi',     
				'body' =>"admin/v_ubah_kata_sandi",
				"data"=>"",
				'footer' => true,
	    );
    	$this->load->view("template_modal",$data);	
  	}
}
