<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>Sign In</title>
        <!-- Favicon-->
        <link rel="icon" href="<?=base_url('public/theme/AdminBSB/')?>favicon.ico" type="image/x-icon">

        <!-- Google Fonts -->
        <link href="<?=base_url('public/theme/AdminBSB/')?>css/fonts.css" rel="stylesheet">
        <link href="<?=base_url('public/theme/AdminBSB/')?>css/material-icons.css" rel="stylesheet">

        <!-- Bootstrap Core Css -->
        <link href="<?=base_url('public/theme/AdminBSB/')?>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

        <!-- Waves Effect Css -->
        <link href="<?=base_url('public/theme/AdminBSB/')?>plugins/node-waves/waves.css" rel="stylesheet" />

        <!-- Animation Css -->
        <link href="<?=base_url('public/theme/AdminBSB/')?>plugins/animate-css/animate.css" rel="stylesheet" />

        <!-- Custom Css -->
        <link href="<?=base_url('public/theme/AdminBSB/')?>css/style.css" rel="stylesheet">

        <!-- sweetalert -->
        <link href="<?=base_url('public/theme/AdminBSB/')?>plugins/sweetalert/sweetalert.css" rel="stylesheet" />

        <style>
            body{                                
                background-color: #77aa77;
                background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='100%25' height='100%25' viewBox='0 0 2 1'%3E%3Cdefs%3E%3ClinearGradient id='a' gradientUnits='userSpaceOnUse' x1='0' x2='0' y1='0' y2='1'%3E%3Cstop offset='0' stop-color='%2377aa77'/%3E%3Cstop offset='1' stop-color='%234fd'/%3E%3C/linearGradient%3E%3ClinearGradient id='b' gradientUnits='userSpaceOnUse' x1='0' y1='0' x2='0' y2='1'%3E%3Cstop offset='0' stop-color='%23cf8' stop-opacity='0'/%3E%3Cstop offset='1' stop-color='%23cf8' stop-opacity='1'/%3E%3C/linearGradient%3E%3ClinearGradient id='c' gradientUnits='userSpaceOnUse' x1='0' y1='0' x2='2' y2='2'%3E%3Cstop offset='0' stop-color='%23cf8' stop-opacity='0'/%3E%3Cstop offset='1' stop-color='%23cf8' stop-opacity='1'/%3E%3C/linearGradient%3E%3C/defs%3E%3Crect x='0' y='0' fill='url(%23a)' width='2' height='1'/%3E%3Cg fill-opacity='0.5'%3E%3Cpolygon fill='url(%23b)' points='0 1 0 0 2 0'/%3E%3Cpolygon fill='url(%23c)' points='2 1 2 0 0 0'/%3E%3C/g%3E%3C/svg%3E");
                background-attachment: fixed;
                background-size: cover;
            }
        </style>
    </head>

    <body class="login-page">         
        <div class="login-box">
            <div class="logo">
                <a href="javascript:void(0);">Sistem Rujukan Online</a>
                <small>Utamakan Kecepatan</small>
            </div>
            <div class="card">
                <div class="body">
                    <form id="sign_in" method="POST">
                        <div class="msg">Silahakan Login</div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">person</i>
                            </span>
                            <div class="form-line">
                                <input id="karyawan_id" type="text" class="form-control" name="karyawan_id" placeholder="ID karyawan" required autofocus>
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line">
                                <input id="karyawan_password" type="password" class="form-control" name="karyawan_password" placeholder="Password" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8 p-t-5">
                                <!-- <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                                <label for="rememberme">Remember Me</label> -->
                            </div>
                            <div class="col-xs-4">
                                <a href="#" id="signin" class="btn btn-block bg-orange waves-effect">SIGN IN</a>
                            </div>
                        </div>
                        <div class="row m-t-15 m-b--20">
                            <div class="col-xs-6">
                                <!-- <a href="sign-up.html">Register Now!</a> -->
                            </div>
                            <div class="col-xs-6 align-right">
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Jquery Core Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/bootstrap/js/bootstrap.js"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/node-waves/waves.js"></script>

        <!-- Validation Plugin Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/jquery-validation/jquery.validate.js"></script>

        <!-- Custom Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>js/admin.js"></script>
        <script src="<?=base_url('public/theme/AdminBSB/')?>js/pages/examples/sign-in.js"></script>

        <!-- sweetalert -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/sweetalert/sweetalert.min.js"></script>

        <script>
            function checkLogin() {
                var dataPost = $("form").serialize();
                $.ajax({
                    url: "<?php echo site_url('Main/checkLogin')?>",
                    type: "POST",
                    data: dataPost,
                    success: function (data) {
                        var obj = jQuery.parseJSON(data)
                        //console.log(obj);
                        var kode =obj.metaData.kode;
                        var pesan =obj.metaData.message;
                        if(kode=="200"){
                            //swal("Login Berhasil",pesan, "success");    
                            document.location.href = '<?php echo site_url('Main/dashboard')?>';
                        }else{
                            swal("Login Gagal",pesan, "error");
                        }
                        
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error koneksi !", "silahkan coba lagi", "error");
                    }
                });
            }

            $(document).on("click","#signin",function(e){
                e.preventDefault();    
                checkLogin();
            });

            $('#karyawan_password').keypress(function(event){	
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if(keycode == '13'){
                    event.preventDefault();
                    checkLogin();
                }
                event.stopPropagation();
            });
            
        </script>
    </body>

</html>