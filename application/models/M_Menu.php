<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_menu extends CI_Model {
	
    public function __construct(){ 
        parent::__construct();
        $this->load->model('M_getPropertyTable', 'property');
    }    

   
    public function getmenuById($menu_id=""){
        $menu = $this->db->get_where('rs_menu', array('menu_id' => $menu_id));
        return $menu->row();
    }

    public function getKategoriMenu(){
        $role = $this->db->get_where('rs_kategori_menu');
        return $role->result();
    }

    public function getmenuAll(){
        $data = array();
                        
                        $this->db->order_by("menu_id","desc");
                        $this->db->join("rs_kategori_menu kategori","kategori.kategori_id=rs_menu.kategori_id","left");
        $qmenu =    $this->db->get("rs_menu");
        
        if($qmenu->num_rows()<1){
            $output = array(                 
                 "data" =>[]
            );
            echo json_encode($output);
            
        }else{

            $n=1;
            foreach($qmenu->result() as $key) {
                $status=($key->menu_status_aktif=="aktif") ? '<span class="label bg-green">'.$key->menu_status_aktif.'</span>':'<span class="label bg-red">'.$key->menu_status_aktif.'</span>';
                $data[] = array(
                    $key->menu_id,
                    $key->menu_nama,
                    rupiah($key->menu_harga),
                    '<img width="50px" height="50px" src="'.base_url('uploads/menu/'.$key->menu_img).'" width="64" />',
                    $status,
                    $key->menu_stok,
                    $key->kategori_nama,
                    '<a href="#'.$key->menu_id.'" id="updatemenu" class="btn bg-black waves-effect">Sunting</a>
                    <a href="#'.$key->menu_id.'" id="deletemenu" class="btn bg-black waves-effect">Hapus</a>',
                );

                $n++;
            }

          $output = array(               
                 "recordsTotal" => $qmenu->num_rows(),
                 "recordsFiltered" => $qmenu->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
        } 
    }
    
    private function _uploadImage($menu_id){
        $config['upload_path']          = './uploads/menu/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['file_name']            = $menu_id;
        $config['overwrite']			= true;
        $config['max_size']             = 1024; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('menu_img')) {
            return $this->upload->data("file_name");
        }

        //print_r($this->upload->display_errors());
        
        return "default.png";
    }

    public function insertDatamenu(){

        //print_r($_POST);exit;

        $menu_id = $this->input->post("menu_id");
        $menu_nama = $this->input->post("menu_nama");
        $menu_tgl_dibuat = $this->input->post("menu_tgl_dibuat");
        $menu_harga = preg_replace("/\D/","", $this->input->post("menu_harga"));
        $old_image = $this->input->post("old_image");
        $menu_deskripsi = $this->input->post("menu_deskripsi");
        $menu_stok = $this->input->post("menu_stok");
        $menu_status_aktif = ($this->input->post("menu_status_aktif")=="on") ? "aktif":"non-aktif";
        $kategori_id = $this->input->post("kategori_id");
        $check=$this->db->get_where("rs_menu",array("menu_id"=>$menu_id));        
        
        
        // if (!$this->upload->do_upload('menu_img')){
        //     $result=array("metaData"=>array("kode"=>"210","message"=>"Upload Gagal ".$this->upload->display_errors()));                
        // }else{                        
        // }
        
        if($check->num_rows()>0){                            
            if (!empty($_FILES["menu_img"]["name"])) {
                $menu_img= $this->_uploadImage($menu_id);
            } else {
                $menu_img = $old_image;
            }

            $update=$this->db->update("rs_menu",array(  "menu_nama"=>$menu_nama,
                                                        "menu_img"=>$menu_img,
                                                        "menu_harga"=>$menu_harga,
                                                        "menu_status_aktif"=>$menu_status_aktif,
                                                        "menu_deskripsi"=>$menu_deskripsi,
                                                        "menu_stok"=>$menu_stok,
                                                        "kategori_id"=>$kategori_id),                                                        
                                                    array("menu_id"=>$menu_id));

            $result=array("metaData"=>array("kode"=>"200","message"=>"Update Data Berhasil"));
        }else{            
            $insert=$this->db->insert("rs_menu",array(  "menu_id"=>$menu_id,
                                                        "menu_nama"=>$menu_nama,
                                                        "menu_img"=>$this->_uploadImage($menu_id),
                                                        "menu_harga"=>$menu_harga,
                                                        "menu_stok"=>$menu_stok,
                                                        "menu_deskripsi"=>$menu_deskripsi,
                                                        "menu_status_aktif"=>$menu_status_aktif,
                                                        "kategori_id"=>$kategori_id));

            $result=array("metaData"=>array("kode"=>"200","message"=>"Simpan Data Berhasil"));
        }
        
        
        return json_encode($result);
    }

    public function createAutoMenuId(){                    
                    $this->db->limit(1);
                    $this->db->order_by("menu_id","desc");
        $result =   $this->db->query('SELECT MAX(RIGHT(menu_id,4)) as max_id FROM rs_menu');        
        $data = $result->row();
        $id_max = $data->max_id;
        $sort_num = (int) substr($id_max, 1, 4);
        $sort_num++;
        $new_code = "M".sprintf("%04s", $sort_num);
        $result=array("metaData"=>array("kode"=>"200","message"=>"Kode berhasil didapatkan"),"response"=>array("menu_id"=>$new_code));
        return json_encode($result);
    }

    public function getmenuBymenuId($menu_id=""){
        $query = $this->db->get_where('rs_menu',array('menu_id'=>$menu_id));
        $data = array();
        $data["datamenu"]=$query->row();
        return json_encode($data); 
    }

    public function deletemenu($menu_id=""){            
        $img=$this->getmenuById($menu_id);        
        $filename = explode(".", $img->menu_img)[0];
    
        $qDeletemenu = $this->db->delete('rs_menu', array('menu_id' => $menu_id));
        $msgSuccess="Hapus menu Berhasil";
        $msgFailed="Hapus menu Gagal";
        if($qDeletemenu){            
            if ($img->menu_img != "default.jpg") {
                $filename = explode(".", $img->menu_img)[0];
                array_map('unlink', glob(FCPATH."uploads/menu/$filename.*"));
            }
            $data["response"]=array("kode"=>"200","message"=>$msgSuccess);    
        }else{
            $data["response"]=array("kode"=>"201","message"=>$msgFailed);    
        }        

        return json_encode($data); //format the array into json data   
    }

}
