<!-- Top Bar -->
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="<?=site_url("Main/dashboard")?>">Sistem Rujukan Online</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Call Search -->
                <!-- <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li> -->
                <!-- #END# Call Search -->
                <!-- Notifications -->
                <li class="dropdown">                    
                    <ul class="dropdown-menu">
                        <li class="header">NOTIFICATIONS</li>
                        <li class="body">
                            <ul class="menu">
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle bg-light-green">
                                            <i class="material-icons">person_add</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4>12 new members joined</h4>
                                            <p>
                                                <i class="material-icons">access_time</i> 14 mins ago
                                            </p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle bg-cyan">
                                            <i class="material-icons">add_shopping_cart</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4>4 sales made</h4>
                                            <p>
                                                <i class="material-icons">access_time</i> 22 mins ago
                                            </p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle bg-red">
                                            <i class="material-icons">delete_forever</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4><b>Nancy Doe</b> deleted account</h4>
                                            <p>
                                                <i class="material-icons">access_time</i> 3 hours ago
                                            </p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle bg-orange">
                                            <i class="material-icons">mode_edit</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4><b>Nancy</b> changed name</h4>
                                            <p>
                                                <i class="material-icons">access_time</i> 2 hours ago
                                            </p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle bg-blue-grey">
                                            <i class="material-icons">comment</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4><b>John</b> commented your post</h4>
                                            <p>
                                                <i class="material-icons">access_time</i> 4 hours ago
                                            </p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle bg-light-green">
                                            <i class="material-icons">cached</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4><b>John</b> updated status</h4>
                                            <p>
                                                <i class="material-icons">access_time</i> 3 hours ago
                                            </p>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);">
                                        <div class="icon-circle bg-purple">
                                            <i class="material-icons">settings</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4>Settings updated</h4>
                                            <p>
                                                <i class="material-icons">access_time</i> Yesterday
                                            </p>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View All Notifications</a>
                        </li>
                    </ul>
                </li>                
            </ul>
        </div>
    </div>
</nav>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="<?=base_url('public/theme/AdminBSB/')?>images/user.png" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?=$this->session->userdata("karyawan_nama");?></div>
                <div class="email"><?=$this->session->userdata("nama_role");?></div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="<?=site_url('admin/C_Karyawan/viewProfile')?>"><i class="material-icons">person</i>Profile</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a data-toggle="modal" data-target="#defaultModal" href="<?=site_url('main/changePass')?>"><i class="material-icons">lock</i>Ubah Password</a></li>
                        
                        <li><a id="keluar" href="#"><i class="material-icons">exit_to_app</i>Keluar</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li class="active">
                    <a href="<?=site_url('main/dashboard')?>">
                        <i class="material-icons">home</i>
                        <span>Home</span>
                    </a>
                </li>
                <?php
                    $id_role = $this->session->userdata("id_role");
                    $qPcategory = "SELECT * FROM
                                        rs_user_role a
                                    INNER JOIN rs_page p ON p.page_id = a.page_id
                                    INNER JOIN rs_page_category pc ON pc.category_id = p.category_id
                                    WHERE  
                                        a.id_role IN (".$id_role.")
                                    AND p.page_active = '1'
                                    GROUP BY
                                        pc.category_id order by ABS(pc.category_sort) ASC";

                    $xPcategory = $this->db->query($qPcategory);

                    $nav='';
                    foreach($xPcategory->result() as $wPcategory) {
                        $qPage = "SELECT * FROM
                                        rs_user_role a
                                    INNER JOIN rs_page p ON p.page_id = a.page_id
                                    INNER JOIN rs_page_category pc ON pc.category_id = p.category_id
                                    WHERE
                                        a.id_role IN (".$id_role.")
                                    AND
                                        p.category_id = '".$wPcategory->category_id."'
                                     AND
                                        p.page_active = '1'
                                    AND 
                                        p.page_parent_id = '0' order by ABS(p.page_sort) ASC";
                        //$qPage = "SELECT * FROM rs_page where page_parent_id = '0' AND category_id = '".$wPcategory->category_id."'";
                        $xPage = $this->db->query($qPage);
                        /*print_r($xPage->num_rows());
                        exit;*/
                        
                        $nav .='  
                                <li>
                                    <a href="javascript:void(0);" class="menu-toggle">
                                        <i class="material-icons">'.@$wPcategory->category_icon.'</i>
                                        <span>'.@$wPcategory->category_title.'</span>
                                    </a>
                                    <ul class="ml-menu">
                                     ';  
                                        //print_r($xPage->num_rows());exit;
                                        foreach($xPage->result() as $wPage) {
                                                                        
                                            $label = $wPage->page_label;
                                            $page_name = $wPage->page_name;
                                            $page_icon= $wPage->page_icon;
                                            
                                             
                                            $parent = $wPage->page_parent_id;
                                            $page_id = $wPage->page_id;
                                            $page_target_blank = $wPage->page_target_blank;
                                            
                                            $qAccess = "SELECT * FROM rs_user_role WHERE page_id = '".$page_id."' AND id_role IN (".$id_role.")";
                                            
                                            //error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
                                            $xAccess = $this->db->query($qAccess);
                                            $rAccess = $xAccess->row();
                                            $page_id_r = $rAccess->page_id;
                
                                            //$selected1 = ($page_id==$page_id_r) ?  "checked" : "";
                
                
                                            $qParentCategoryCek = $this->db->query("SELECT * FROM rs_page WHERE page_parent_id = '".$wPage->page_id."' AND page_active = '1' order by ABS(page_sort) asc");
                                            
                                            
                                            
                                            /*print_r($this->db->last_query());exit;*/
                                            
                                            //$ketemu = $qParentCategory->row();
                                            $ketemu = $qParentCategoryCek->num_rows();
                                            /*print_r($ketemu);
                                            exit;
                                            */
                                            if($page_name!="#"):        
                                                //$nav .='<li><a href="'.$page_name.'"><i class=" icon-arrow-right22"></i>'.$label.'</a></li>';
                                                if($page_target_blank=="0"){
                                                    $nav .='<li>
                                                                <a href="'.site_url($page_name).'" class="">
                                                                <i class="material-icons">'.$page_icon.'</i>
                                                                <span>'.$label.'</span>
                                                                </a>
                                                            </li>';
                                                }else{
                                                    $nav .='<li>
                                                                <a href="'.base_url($page_name).'" target="_blank" class="">
                                                                <i class="material-icons">'.$page_icon.'</i>
                                                                <span>'.$label.'</span>
                                                                </a>
                                                            </li>';
                                                }
                                                
                                            endif;
                                            
                                            if ($ketemu > 0) {
                                                
                                                $nav .='<li>                                                
                                                            <a class="">
                                                                <i class="material-icons">'.$page_icon.'</i>
                                                                <span>'.$label.'</span>
                                                            <ul>';
                                                
                                                $qParentCategory = $this->db->query("SELECT * FROM
                                                    rs_user_role
                                                    LEFT JOIN rs_page ON rs_page.page_id = rs_user_role.page_id
                                                    LEFT JOIN rs_page_category ON rs_page_category.category_id = rs_page.category_id
                                                    WHERE 
                                                    rs_page.page_active='1' and rs_page.page_parent_id ='{$page_id}' and 
                                                    rs_user_role.id_role IN (".$id_role.") order by ABS(page_sort) asc
                                                ");
                                                          
                                                                
                                                foreach ($qParentCategory->result() as $wParent) {
                                                    
                                                    $page_name_parent = ($wParent->page_id!=71 && $wPcategory->category_id==7 || $wPcategory->category_id==12 || $wPcategory->category_id==13) ? $wParent->page_name."".$data->lokasi_id : $wParent->page_name;
                                                    
                                                    $page_name_parent_new = ($wParent->page_id==71) ? $wParent->page_name:$page_name_parent;
                                                    
                                                    $label2 = $wParent->page_label;
                                                    $page_id_2 = $wParent->page_id;
                                                    $page_icon_2 = $wParent->page_icon;
                                                    $page_target_blank_2 = $wParent->page_target_blank;    
                                            
                                                    
                                                    //$nav .='<li><a href="'.$wParent->page_name.'">&nbsp;&nbsp;&nbsp;'.$label2.'</a></li>';
                                                    if($page_target_blank_2=="0"){
                                                         $nav .='<li>
                                                                <a href="'.site_url($page_name_parent_new).'" class="">
                                                                <i class="material-icons">'.$page_icon_2.'</i>
                                                                <span>'.$label2.'</span>
                                                                </a>
                                                            </li>';
                                                    }else{
                                                        $nav .='<li>
                                                            <a href="'.base_url($page_name_parent_new).'" target="_blank" class="">
                                                                <i class="material-icons">'.$page_icon_2.'</i>
                                                                <span>'.$label2.'</span>
                                                            </a>
                                                        </li>';
                                                    }
                                                }
                
                                                $nav .='</ul>
                                                </li>';
                                                
                                            }
                                        }
                                    $nav .='</ul>
                                </li>';
                    }
                    echo $nav;                        
                    
                ?>

            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2019 <a href="javascript:void(0);">Rizky Saraan</a>.
            </div>
            <div class="version">
                <b>Version: </b> 1.0.0
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
    
</section>

<script>
    $(document).on("click","#keluar",function(e){
        e.preventDefault();    
            swal({
            title: "Apakah kamu yakin ?",
            text: "Kamu akan keluar dari aplikasi",
            type: "warning",
            showCancelButton: true,
            cancelButtonColor: "#F44336",
            confirmButtonColor: "#F44336",
            confirmButtonText: "Yes",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function (isConfirm) {
            if (!isConfirm) return;
            $.ajax({
                url: "<?php echo site_url('main/logout')?>",
                type: "POST",
                success: function (data) {
                    var obj = jQuery.parseJSON(data)
                    //console.log(obj);
                    var kode =obj.metaData.kode;
                    var pesan =obj.metaData.message;
                    if(kode=="200"){
                        swal("Keluar Berhasil",pesan, "success");    
                        document.location.href = '<?php echo site_url('main')?>';
                    }else{
                        swal("Keluar Gagal",pesan, "error");
                    }
                    
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error koneksi !", "silahkan coba lagi", "error");
                },
            });
        });   
    });
</script>