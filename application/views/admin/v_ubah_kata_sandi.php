<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <form>
        <label for="email_address">Kata sandi lama</label>
        <div class="form-group">
            <div class="form-line">
                <input type="password" id="password_lama" name="password_lama" class="form-control" placeholder="Masukan kata sandi lama">
            </div>
        </div>
        <label for="password">Kata sandi baru</label>
        <div class="form-group">
            <div class="form-line">
                <input type="password" name="password_baru" id="password_baru" class="form-control" placeholder="Masukan kata sandi baru">
            </div>
        </div>
        <label for="password">Ulangi Kata sandi baru</label>
        <div class="form-group">
            <div class="form-line">
                <input type="password" name="password_konfirmasi_baru" id="password_konfirmasi_baru" class="form-control" placeholder="Masukan Ulangi kata sandi baru">
            </div>
        </div>
        <br>
        <!-- <a id="save" href="#" class="btn btn-primary m-t-15 waves-effect">Ubah</a> -->
    </form>
</div>   
<script>
    $(document).on("change","#password_baru",function(){
        var length = $("#password_baru").val().length;
        if(length<6){            
            swal("Error","Kata sandi terlalu pendek, minimum 6 karakter", "error");
            $("#password_baru").focus();
            $("#save").hide(100);
        }else{
            $("#save").show(100);
        }
    });
    $(document).on("change","#password_konfirmasi_baru",function(){
        cekEqualPassword();
    });

    $(document).on("change","#password_baru",function(){
        if($("#password_konfirmasi_baru").val()!=""){
            cekEqualPassword();
        }
    });

    function cekEqualPassword(){
        var password_konfirmasi_baru = $("#password_konfirmasi_baru").val();
        var password_baru = $("#password_baru").val();
        if(password_konfirmasi_baru!=password_baru){
            swal("Error","Kata sandi Baru tidak sama dengan Ulangi Kata Sandi", "error");
            $("#password_konfirmasi_baru").focus();
            $("#save").hide(100);
        }else{
            $("#save").show(100);
        }
    }

    function changePassword() {
        var dataPost = $("form").serialize();
        swal({
            title: "Apakah kamu yakin ?",
            text: "Kamu akan Mengganti password untuk masuk ke sistem ?",
            type: "info",
            showCancelButton: true,
            cancelButtonColor: "#F44336",
            confirmButtonColor: "#4CAF50",
            confirmButtonText: "Yes",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function (isConfirm) {
            if (!isConfirm) return;
            $.ajax({
                url: "<?php echo site_url('Main/actChangePass')?>",
                type: "POST",
                data: dataPost,
                success: function (data) {
                    var obj = jQuery.parseJSON(data)
                    //console.log(obj);
                    var kode =obj.metaData.kode;
                    var pesan =obj.metaData.message;
                    if(kode=="200"){
                        swal({
                            title: "Berhasil",
                            text: pesan,
                            type: "success",
                            confirmButtonText: "OK",
                            closeOnConfirm: false,
                            showLoaderOnConfirm: true,
                        }, function (isConfirm) {
                            if (!isConfirm) return;
                            document.location.href = '<?php echo site_url('Main/autoLogout')?>';
                        });                        
                        
                    }else{
                        swal("Gagal",pesan, "error");
                    }
                    
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error koneksi !", "silahkan coba lagi", "error");
                }
            });
        });
    }

    $(document).on("click","#save",function(e){
        e.preventDefault();    
        changePassword();
    });
</script>