<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Menu extends CI_Controller {

  	public function __construct(){ 
      	parent::__construct();     
      	$this->load->model('M_menu','menu');
  	}    

	public function index(){
	    $data=array(
	            'body'=>'v_daftar_menu',
	            'title'=>'Daftar menu', 
	            'navbar'=>'admin',
	            'index'=>'Daftar menu',
    	        'controller'=>'C_Menu',
				'act'=>'daftar',
				"dataKategoriMenu"=>$this->menu->getKategoriMenu(),
			);  
			
		//print_r($data);exit; 
		$this->load->view('template_default',$data);
	}

	public function getmenuAll(){
		$this->menu->getmenuAll();
	}

	public function getmenuId(){
		$response = $this->menu->createAutoMenuId();	
		echo $response;
	}

	public function getmenuBymenuId($menu_id=""){
		$datamenu = $this->menu->getmenuBymenuId($menu_id);
		echo $datamenu;
	}
	
	public function deletemenu($menu_id){
		$response = $this->menu->deletemenu($menu_id);	
		echo $response;
	}
	public function insertDatamenu(){
		$response = $this->menu->insertDatamenu();	
		echo $response;
	}
}
