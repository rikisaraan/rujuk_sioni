<?php
    if($this->session->userdata('karyawan_nama')==""){
        echo '  <script>
                    alert("Maaf Session anda sudah habis silahkan login kembali"); 
                    document.location="'.site_url("main/autoLogout").'";
                </script>';
    }

    $page = $_SERVER['PHP_SELF'];
    $sec = "600";  
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title><?=$title?></title>
        <!-- Favicon-->
        <link rel="icon" href="<?=base_url('public/theme/AdminBSB/')?>favicon.ico" type="image/x-icon">

        <!-- Google Fonts -->
        <link href="<?=base_url('public/theme/AdminBSB/')?>css/fonts.css" rel="stylesheet">
        <link href="<?=base_url('public/theme/AdminBSB/')?>css/material-icons.css" rel="stylesheet">
 
        <!-- Bootstrap Core Css -->
        <link href="<?=base_url('public/theme/AdminBSB/')?>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

        <!-- Waves Effect Css -->
        <link href="<?=base_url('public/theme/AdminBSB/')?>plugins/node-waves/waves.css" rel="stylesheet" />

        <!-- Animation Css -->
        <link href="<?=base_url('public/theme/AdminBSB/')?>plugins/animate-css/animate.css" rel="stylesheet" />

        <!-- Morris Chart Css-->
        <link href="<?=base_url('public/theme/AdminBSB/')?>plugins/morrisjs/morris.css" rel="stylesheet" />

        <!-- Custom Css -->
        <link href="<?=base_url('public/theme/AdminBSB/')?>css/style.css" rel="stylesheet">

        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        <link href="<?=base_url('public/theme/AdminBSB/')?>css/themes/all-themes.css" rel="stylesheet" />

        <!-- Data tables Css-->    
        <link href="<?=base_url('public/theme/AdminBSB/')?>plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

                
        <!-- Font awesome-->    
        <link href="<?=base_url('public/theme/AdminBSB/')?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">        

        <!-- sweetalert -->
        <link href="<?=base_url('public/theme/AdminBSB/')?>plugins/sweetalert/sweetalert.css" rel="stylesheet" />

        <!-- Bootstrap Select Css -->
        <link href="<?=base_url('public/theme/AdminBSB/')?>plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
        
        <!-- Bootstrap DatePicker Css -->
        <link href="<?=base_url('public/theme/AdminBSB/')?>plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />

        <!-- Bootstrap Colorpicker Css -->
        <link href="<?=base_url('public/theme/AdminBSB/')?>plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" />
        
        <!-- Bootstrap Material Datetime Picker Css -->
        <link href="<?=base_url('public/theme/AdminBSB/')?>plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

        <!-- Jquery Core Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/bootstrap/js/bootstrap.js"></script>
         
        <!-- Bootstrap color picker Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
                    
        <!-- Ckeditor -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/ckeditor/ckeditor.js"></script>        

        <!-- Validation Plugin Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/jquery-validation/jquery.validate.js"></script>

        <!-- animasi css -->
        <link href="<?=base_url('public/theme/AdminBSB/')?>plugins/animate-css/animate.css" rel="stylesheet" />

        <style>
            body{
                background-color: #ffffff;
            }
        </style>
    </head>

    <body class="theme-green ls-closed">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="preloader">
                    <div class="spinner-layer pl-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                <p>Please wait...</p>
            </div>
        </div>
        <!-- #END# Page Loader -->
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- #END# Overlay For Sidebars -->
        <!-- Search Bar -->
        <div class="search-bar">
            <div class="search-icon">
                <i class="material-icons">search</i>
            </div>
            <input type="text" placeholder="START TYPING...">
            <div class="close-search">
                <i class="material-icons">close</i>
            </div>
        </div>
        <!-- #END# Search Bar -->
        
        
        <?php $this->load->view("v_navbar");?>
        
        <section class="content">
            <div class="container-fluid">
                <?php
                    $detail=isset($detail) ? $detail:"" ;
                    $c_detail=isset($c_detail) ? $c_detail:"" ;
                    $v_detail=isset($v_detail) ? $v_detail:"" ;
                    $t_detail=isset($t_detail) ? $t_detail:"" ;
                    
                    $v_detail_add=isset($v_detail_add) ? $v_detail_add:"" ;
                    $t_detail_add=isset($t_detail_add) ? $t_detail_add:"" ;
                
                    if($navbar=="admin"){
                        echo $this->breadcumb->show($controller,$title,$index,$act,$detail,$c_detail,$v_detail,$t_detail,$v_detail_add,$t_detail_add);
                    }                
                    $this->load->view("admin/".$body, (isset($data) ? $data : array()) );
                ?>
            </div>
        </section>

        <!-- Large Size -->
        <div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content"></div>
            </div>
        </div>
        <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content modal-lg"></div>
            </div>
        </div>

        <div class="modal fade" id="modalLoader" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-body align-center">
                        <p>Mohon Tunggu ...</p>
                        <div class="preloader pl-size-xl">
                            <div class="spinner-layer">
                                <div class="circle-clipper left">
                                    <div class="circle"></div>
                                </div>
                                <div class="circle-clipper right">
                                    <div class="circle"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    

        <!-- Moment Plugin Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/momentjs/moment.js"></script>
        
        <!-- Select Plugin Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/bootstrap-select/js/bootstrap-select.js"></script>
        
        <!-- Bootstrap Datepicker Plugin Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>

        <!-- Slimscroll Plugin Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/node-waves/waves.js"></script>

        <!-- Jquery CountTo Plugin Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/jquery-countto/jquery.countTo.js"></script>

        <!-- Morris Plugin Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/raphael/raphael.min.js"></script>
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/morrisjs/morris.js"></script>
        
        <!-- Sparkline Chart Plugin Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/jquery-sparkline/jquery.sparkline.js"></script>

        <!-- Autosize Plugin Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/autosize/autosize.js"></script>
    

        <!-- Bootstrap Material Datetime Picker Plugin Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

        <!-- Custom Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>js/admin.js"></script>
        <script src="<?=base_url('public/theme/AdminBSB/')?>js/pages/forms/basic-form-elements.js"></script>
        
        <!-- tooltip -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>js/pages/ui/tooltips-popovers.js"></script>

        <!-- Input Mask Plugin Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>

        <!-- Demo Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>js/demo.js"></script>


        <!-- Jquery Datatable-->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/jquery-datatable/jquery.dataTables.js"></script>

        <!-- Jquery DataTable Plugin Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/jquery-datatable/jquery.dataTables.js"></script>
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
    
        <!-- modal -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>js/pages/ui/modals.js"></script>
        
        <!-- sweetalert -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/sweetalert/sweetalert.min.js"></script>

        <!-- Input Mask Plugin Js -->
        <script src="<?=base_url('public/theme/AdminBSB/')?>plugins/rupiah.js"></script>
        <script src="<?=base_url('public/theme/AdminBSB/')?>app.js"></script>

    
        <script>            
            $(document).ready(function(){
                var url = window.location;
                
                // for sidebar menu entirely but not cover treeview
                $('ul.list a').filter(function() {
                return this.href == url;
                }).parent().addClass('active');

                // for treeview
                $('ul.menu-toggle a').filter(function() {
                return this.href == url;
                }).parentsUntil(".list > .menu-toggle > .ml-menu").addClass('active');
        
                $('.year').datepicker({
                    format: "yyyy",
                    viewMode: "years", 
                    minViewMode: "years"
                });

                // // Format mata uang.
                // $( '.rupiah' ).mask('0.000.000.000', {reverse: true});

                // // Format nomor HP.
                // $( '.no_hp' ).mask('0000−0000−0000');
                var $demoMaskedInput = $('#form');
                //Mobile Phone Number
                $demoMaskedInput.find('.mobile-phone-number').inputmask('(999) 999-999-999', { placeholder: '(___) ___-___-___' });
  
                                
            });

            $('.colorpicker').colorpicker();

            function convertToRupiah(angka){
                var rupiah = '';		
                var angkarev = angka.toString().split('').reverse().join('');
                for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
                return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
            }
        </script>

    
    </body>

</html>
