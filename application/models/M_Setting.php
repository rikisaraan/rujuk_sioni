<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_setting extends CI_Model {
	
    public function __construct(){ 
        parent::__construct();
    }    
   
    public function getCategoryPage(){
        $setting = $this->db->get('rs_page_category');        
        return $setting->result();
    }

    public function updateAccessUser(){
        $page_id = $this->input->post("page_id");
        $id_role = $this->input->post("id_role");
        
        $CheckAccessUser = $this->db->delete('rs_user_role', array('id_role'=>$id_role));
        
        foreach($page_id as $id){            
            $UpdateAccessUser = $this->db->insert('rs_user_role', array('page_id'=>$id,'id_role'=>$id_role));
        }
                
        if($UpdateAccessUser){            
            $result=array("metaData"=>array("kode"=>"200","message"=>"Akses User berhasil diperbarui"));
        }else{
            $result=array("metaData"=>array("kode"=>"210","message"=>"Tidak ada akses yang diperbarui"));
        }        

        return json_encode($result);
    }

}
