<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Karyawan extends CI_Model {
	
    public function __construct(){ 
        parent::__construct();
        $this->load->library('encrypt');
    }    
    
    public function checkLogin(){
        $karyawan_id = $this->input->post("karyawan_id");
        $karyawan_password = $this->input->post("karyawan_password");
                    
                    $this->db->join("rs_role role","role.id_role=rs_karyawan.id_role","left");
        $karyawan = $this->db->get_where('rs_karyawan', array('karyawan_id' => $karyawan_id));
        //print_r($this->db->last_query());exit;
        $data=""; 
        
        if($karyawan->num_rows() == 1):
            if($this->encrypt->decode($karyawan->row()->karyawan_password)==$karyawan_password):                

                $data=$karyawan->row();
                $session = array(
                                    "karyawan_id"=>$data->karyawan_id,
                                    "karyawan_nama"=>$data->karyawan_nama,
                                    "karyawan_tgl_lahir"=>$data->karyawan_tgl_lahir,
                                    "karyawan_telp"=>$data->karyawan_telp,
                                    "karyawan_alamat"=>$data->karyawan_alamat,
                                    "karyawan_tgl_daftar"=>$data->karyawan_tgl_daftar,
                                    "karyawan_photo"=>$data->karyawan_photo,
                                    "id_role"=>$data->id_role,
                                    "nama_role"=>$data->nama_role,
                                    'logged_in' =>TRUE
                                );
                   
                $this->session->set_userdata($session);
                $result=array("metaData"=>array("kode"=>"200","message"=>"Login Berhasil"));
            else:
                
                $result=array("metaData"=>array("kode"=>"201","message"=>"Login Gagal password yang anda masukan tidak sesuai"));
            endif;
        else:
            $result=array("metaData"=>array("kode"=>"201","message"=>"Login Gagal Karyawan ID yang anda masukan tidak ditemukan"));
        endif;

        
        return json_encode($result); //format the array into json data   
    }

    public function logout(){
        $this->session->sess_destroy();
        $result=array("metaData"=>array("kode"=>"200","message"=>"Anda Berhasil keluar dari aplikasi"));
        return json_encode($result);
    }
   
    public function getKaryawanById($karyawan_id=""){
                    $this->db->join("rs_role role","role.id_role=rs_karyawan.id_role","left");
        $karyawan = $this->db->get_where('rs_karyawan', array('karyawan_id' => $karyawan_id));
        return $karyawan->row();
    }

    public function getRole(){
        $role = $this->db->get_where('rs_role');
        return $role->result();
    }

    public function getKaryawanAll(){
        $data = array();
                        
                        $this->db->order_by("karyawan_id","desc");
                        $this->db->join("rs_role role","role.id_role=rs_karyawan.id_role","left");
        $qKaryawan =    $this->db->get("rs_karyawan");
        
        if($qKaryawan->num_rows()<1){
            $output = array(                 
                 "data" =>[]
            );
            echo json_encode($output);
            
        }else{

            $n=1;
            foreach($qKaryawan->result() as $key) {
                
                $data[] = array(
                    $key->karyawan_id,
                    $key->karyawan_nama,
                    dateIndo($key->karyawan_tgl_lahir),
                    $key->karyawan_telp,                    
                    $key->karyawan_alamat,
                    dateIndo($key->karyawan_tgl_daftar),
                    $key->nama_role,
                    '<a href="#'.$key->karyawan_id.'" id="updateKaryawan" class="btn bg-black waves-effect">Sunting</a>
                    <a href="#'.$key->karyawan_id.'" id="deleteKaryawan" class="btn bg-black waves-effect">Hapus</a>',
                );

                $n++;
            }

          $output = array(               
                 "recordsTotal" => $qKaryawan->num_rows(),
                 "recordsFiltered" => $qKaryawan->num_rows(),
                 "data" => $data
            );
          echo json_encode($output);
        }
    }

    public function actChangePass(){
        $karyawan_id = $this->session->userdata("karyawan_id");
        $datakaryawan=  $this->db->get_where("rs_karyawan",array('karyawan_id'=>$karyawan_id))->row();
        $password_lama=$this->input->post("password_lama");
        $password_baru=$this->input->post("password_baru");
        $password_konfirmasi_baru=$this->input->post("password_konfirmasi_baru");

        if($this->encrypt->decode($datakaryawan->karyawan_password)==$password_lama){
            $postData=array("karyawan_password"=>$this->encrypt->encode($password_baru));
            $this->db->update('rs_karyawan',$postData,array('karyawan_id'=>$karyawan_id));
            $result=array("metaData"=>array("kode"=>"200","message"=>"Kata sandi berhasil diperbarui silahkan login kembali"));
        }else{
            $result=array("metaData"=>array("kode"=>"410","message"=>"Kata sandi lama anda salah"));
        }
        return json_encode($result);
    }



    public function insertDataKaryawan(){
        //print_r($_POST);exit;
    
        $karyawan_id = $this->input->post("karyawan_id");
        $karyawan_nama = $this->input->post("karyawan_nama");
        $karyawan_tgl_lahir = $this->input->post("karyawan_tgl_lahir");
        $karyawan_telp = preg_replace("/([^0-9]+)/",'',$this->input->post("karyawan_telp"));
        $karyawan_alamat = $this->input->post("karyawan_alamat");
        $karyawan_photo = $this->input->post("karyawan_photo");
        $karyawan_password = $this->input->post("karyawan_password");
        $id_role = $this->input->post("id_role");
        $check=$this->db->get_where("rs_karyawan",array("karyawan_id"=>$karyawan_id));        

        if($check->num_rows()>0){            
            $update=$this->db->update("rs_karyawan",array(  "karyawan_nama"=>$karyawan_nama,
                                                            "karyawan_tgl_lahir"=>$karyawan_tgl_lahir,
                                                            "karyawan_telp"=>$karyawan_telp,
                                                            "karyawan_alamat"=>$karyawan_alamat,
                                                            "karyawan_password"=>$this->encrypt->encode($karyawan_password),
                                                            "karyawan_tgl_daftar"=>date("Y-m-d H:i:s"),
                                                            "id_role"=>$id_role),
                                                    array("karyawan_id"=>$karyawan_id));

            $result=array("metaData"=>array("kode"=>"200","message"=>"Update Data Berhasil"));
        }else{
            $insert=$this->db->insert("rs_karyawan",array(  "karyawan_id"=>$karyawan_id,
                                                            "karyawan_nama"=>$karyawan_nama,
                                                            "karyawan_tgl_lahir"=>$karyawan_tgl_lahir,
                                                            "karyawan_telp"=>$karyawan_telp,
                                                            "karyawan_alamat"=>$karyawan_alamat,
                                                            "karyawan_tgl_daftar"=>date("Y-m-d H:i:s"),
                                                            "karyawan_password"=>$this->encrypt->encode($karyawan_password),
                                                            "id_role"=>$id_role));

            $result=array("metaData"=>array("kode"=>"200","message"=>"Simpan Data Berhasil"));
        }


        
        return json_encode($result);
    }

    public function createAutoNik(){                    
                    $this->db->limit(1);
                    $this->db->order_by("karyawan_id","desc");
        $result =   $this->db->query('SELECT MAX(RIGHT(karyawan_id, 4)) as max_id FROM rs_karyawan');        
        $data = $result->row();
        $id_max = $data->max_id;
        $sort_num = (int) substr($id_max, 1, 4);
        $sort_num++;
        $new_code = "K".date("ymd").sprintf("%04s", $sort_num);
        $result=array("metaData"=>array("kode"=>"200","message"=>"Kode berhasil didapatkan"),"response"=>array("karyawan_id"=>$new_code));
        return json_encode($result);
    }

    public function getKaryawanByKaryawanId($karyawan_id=""){
        $query = $this->db->get_where('rs_karyawan',array('karyawan_id'=>$karyawan_id));
        $data = array();
        $row=$query->row();
        //$data["dataKaryawan"]=$query->row();
        $data["dataKaryawan"]["karyawan_id"] =$row->karyawan_id;
        $data["dataKaryawan"]["karyawan_nama"] =$row->karyawan_nama;
        $data["dataKaryawan"]["karyawan_tgl_lahir"] =$row->karyawan_tgl_lahir;
        $data["dataKaryawan"]["karyawan_telp"] =$row->karyawan_telp;
        $data["dataKaryawan"]["karyawan_alamat"] =$row->karyawan_alamat;
        $data["dataKaryawan"]["karyawan_tgl_daftar"] =$row->karyawan_tgl_daftar;
        $data["dataKaryawan"]["karyawan_photo"] =$row->karyawan_photo;
        $data["dataKaryawan"]["karyawan_password"] =$this->encrypt->decode($row->karyawan_password);
        $data["dataKaryawan"]["id_role"]=$row->id_role;        
        return json_encode($data); 
    }

    public function deleteKaryawan($karyawan_id=""){
        $qDeleteKaryawan = $this->db->delete('rs_karyawan', array('karyawan_id' => $karyawan_id));
        $msgSuccess="Hapus Karyawan Berhasil";
        $msgFailed="Hapus Karyawan Gagal";
        if($qDeleteKaryawan){
            $data["response"]=array("kode"=>"200","message"=>$msgSuccess);    
        }else{
            $data["response"]=array("kode"=>"201","message"=>$msgFailed);    
        }        

        return json_encode($data); //format the array into json data   
    }

}
