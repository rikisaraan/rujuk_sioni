<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_getPropertyTable extends CI_Model {
	
    public function __construct(){ 
        parent::__construct();
    }    
    
    public function getProperty($id,$namaTable,$primaryKey){
        $o = $this->db->limit(1)->get_where($namaTable,array($primaryKey=>$id))->row_array();		
		$fieldName = $this->db->query($this->db->last_query())->list_fields();
		$formField = array();		
		foreach($fieldName as $a){
			$formField[$a]=set_value($a, isset($o[$a]) ? $o[$a] : $this->input->post($a));
		}		
		return (object) $formField;
        
    }
}
