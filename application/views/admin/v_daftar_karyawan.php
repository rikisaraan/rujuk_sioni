<div class="card">
    <div class="header bg-green">
        <h2>Form Karyawan</h2>
    </div>
    <div class="body">
        <form id="form">
            <div class="row clearfix">
                <div class="col-md-6">
                    <label for="karyawan_id">Karyawan ID</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" id="karyawan_id" name="karyawan_id" class="form-control" readonly>
                        </div>
                    </div>

                    <label for="karyawan_tgl_lahir">Tanggal Lahir</label>
                    <div class="form-group">
                        <div class="form-line" id="bs_datepicker_container">
                            <input  id="karyawan_tgl_lahir" name="karyawan_tgl_lahir" type="text" class="form-control" placeholder="Pilih tanggal lahir" data-date-format="yyyy-mm-dd">
                        </div>
                    </div>

                    <label for="karyawan_password">Password</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="password" id="karyawan_password" name="karyawan_password" class="form-control" placeholder="Minimal 6 karakter">
                        </div>
                    </div>

                    <label for="id_role">Akses</label>
                    <div class="form-group">
                        <select class="form-control show-tick" data-live-search="true" name="id_role" id="id_role">
                            <option value="" disabled>Silahkan Pilih Akses Karyawan</option>
                            <?php
                                foreach ($dataRole as $role) {
                                    echo"<option value='".$role->id_role."'>".$role->nama_role."</option>";
                                }
                            ?>
                        </select>                
                    </div>                                        
                </div>
                <div class="col-md-6">
                    <label for="karyawan_nama">Nama Karyawan</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" id="karyawan_nama" name="karyawan_nama" class="form-control" placeholder="Masukan Nama Karyawan">
                        </div>
                    </div>

                    <label for="karyawan_telp">Telepon</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" id="karyawan_telp" name="karyawan_telp" class="form-control mobile-phone-number" placeholder="Ex: (000) 000-00-00">
                        </div>
                    </div>
                    
                    <label for="karyawan_alamat">Alamat</label>
                    <div class="form-group">
                        <div class="form-line">
                            <textarea id="karyawan_alamat" name="karyawan_alamat" rows="5" class="form-control no-resize" placeholder="Ex: jalan sulawesi bloka9/30 Bintaro, Tangerang Selatan, Banten"></textarea>
                        </div>
                    </div>
                    
                    <a id="saveForm" href="javascript:void(0);" class="bg-black btn waves-effect">Simpan</a>
                    <!-- <input type="submit" class="bg-black btn waves-effect" value="Simpan"/> -->
                    
                </div>
            </div>
        </form>
    </div>
</div>

<div class="card">
    <div class="header bg-green">
        <h2>
            <?=$title?>
        </h2>
    </div>
    <div class="body">
        <div class="table-responsive">
            <table id="tableAjaxKaryawan" class="table table-bordered table-striped table-hover dataTable js-exportable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nama</th>
                        <th>Tanggal Lahir</th>
                        <th>Telepon</th>
                        <th>Alamat</th>
                        <th>Tanggal Daftar</th>
                        <th>Akses</th>
                        <th width="17%">#</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {        
        getKaryawanAll();        
    });
    
    function getKaryawanId(){
        $.ajax({ 
			url: "<?php echo site_url("admin/C_Karyawan/getKaryawanId")?>",
			dataType: "json",
			type: "GET",
			success: function(data){
                $("#karyawan_id").val(data.response.karyawan_id);
			}
     	});
    }

    function getKaryawanAll(){        
        $("#tableAjaxKaryawan").DataTable( {
            "destroy": true,
            "bLengthChange": true,
            "filter": true,
            "dom": 'Bfrtip',
            buttons: [            
                {
                    extend: 'copy',
                    messageTop: 'Copy Data Karyawan',
                    className: 'btn bg-grey waves-effect'
                },
                {
                    extend: 'excel',
                    messageTop: 'Data karyawan',
                    className: 'btn bg-green waves-effect'
                },
                {
                    extend: 'pdf',
                    messageBottom: null,
                    className: 'btn bg-red waves-effect'
                },
                {
                    extend: 'print',
                    className: 'btn bg-black waves-effect',
                    messageTop: function () {
                        printCounter++;
     
                        if ( printCounter === 1 ) {
                            return 'This is the first time you have printed this document.';
                        }
                        else {
                            return 'You have printed this document '+printCounter+' times';
                        }
                    },
                    messageBottom: null
                }
            ],
            "ajax": {
                url : "<?php echo site_url("admin/C_Karyawan/getKaryawanAll") ?>",
                type: "GET",
            },
        } );    
        getKaryawanId();
    }
    
    $(document).on("click","#saveForm",function(){                        
        if($("#karyawan_nama").val()==""){
            swal("Informasi","Nama Karyawan tidak boleh kosong", "info");
            return false;
        }
        if($("#karyawan_tgl_lahir").val()==""){
            swal("Informasi","Tanggal lahir Karyawan tidak boleh kosong", "info");
            return false;
        }
        if($("#karyawan_telp").val()==""){
            swal("Informasi","No Telp Karyawan tidak boleh kosong", "info");
            return false;
        }
        if($("#karyawan_alamat").val()==""){
            swal("Informasi","Alamat Karyawan tidak boleh kosong", "info");
            return false;
        }
        if($("#karyawan_password").val()==""){
            swal("Informasi","Password tidak boleh kosong", "info");
            return false;
        }
        
        insertKaryawan();
    });

    $(document).on("click","#updateKaryawan",function(e){
        e.preventDefault();
        var karyawan_id = $(this).attr('href').replace(/^.*?(#|$)/,'');	
        getKaryawanByKaryawanId(karyawan_id);
        //console.log(karyawan_id);
    });

    $(document).on("click","#deleteKaryawan",function(e){
        e.preventDefault();
        var karyawan_id = $(this).attr('href').replace(/^.*?(#|$)/,'');	
        deleteKaryawan(karyawan_id);
        getKaryawanAll();
    });

    function insertKaryawan(){        
        swal({
            title: "Apakah kamu yakin ?",
            text: "Kamu akan menyimpan data ini",
            type: "info",
            showCancelButton: true,
            cancelButtonColor: "#F44336",
            confirmButtonColor: "#4CAF50",
            confirmButtonText: "Yes",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function (isConfirm) {
            if (!isConfirm) return;
            $.ajax({
                url: "<?php echo site_url('admin/C_Karyawan/insertDataKaryawan')?>",
                type: "POST",
                data: $("form").serialize(),
                success: function (data) {
                    var obj = jQuery.parseJSON(data)
                    //console.log(data);
                    var kode =obj.metaData.kode;
                    var pesan =obj.metaData.message;
                    if(kode=="200"){
                        swal("Berhasil",pesan, "success");                            
                        scrollToBot();
                        getKaryawanAll();
                        resetInput();
                    }else{
                        swal("Simpan Gagal",pesan, "error");
                    }
                    
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error koneksi !", "silahkan coba lagi "+thrownError, "error");
                }
            });
        });
    }

    function deleteKaryawan(karyawan_id){
        swal({
            title: "Apakah kamu yakin ?",
            text: "Kamu akan menghapus data ini",
            type: "info",showCancelButton: true,
            cancelButtonColor: "#F44336",
            confirmButtonColor: "#4CAF50",
            confirmButtonText: "Yes",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function (isConfirm) {
            if (!isConfirm) return;
            $.ajax({
                url: "<?php echo site_url("admin/C_Karyawan/deleteKaryawan")?>/"+karyawan_id,
                dataType: "json",
                type: "GET",
                success: function (data) {
                    var kode = data.response.kode;              
				    var message = data.response.message;
                    if(kode=="200"){
                        swal("Berhasil",message, "success");                            
                        scrollToBot();
                        getKaryawanAll();
                        resetInput();
                    }else{
                        swal("Hapus data gagal",message, "error");
                    }
                    
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error koneksi !", "silahkan coba lagi "+thrownError, "error");
                }
            });
        });    
    }
    
    function getKaryawanByKaryawanId(karyawan_id){
		
		$.ajax({ 
			url: "<?php echo site_url("admin/C_Karyawan/getKaryawanByKaryawanId")?>/"+karyawan_id,
			dataType: "json",
			type: "GET",
			success: function(data){
                $("#karyawan_id").val(data.dataKaryawan.karyawan_id);
                $("#karyawan_tgl_lahir").val(data.dataKaryawan.karyawan_tgl_lahir);
                $("#karyawan_password").val(data.dataKaryawan.karyawan_password);
                $("#karyawan_nama").val(data.dataKaryawan.karyawan_nama);
                $("#karyawan_telp").val(data.dataKaryawan.karyawan_telp);
                $("#karyawan_alamat").val(data.dataKaryawan.karyawan_alamat);
				$("#id_role").val(data.dataKaryawan.id_role).change();
			  
			  	scrollToTop();
			}
     	});
    }


    
    function scrollToTop(){
        $("html, body").animate({ scrollTop: 20 }, "slow");        
    }
    function scrollToBot(){
        $("html, body").animate({ scrollTop: 1000 }, "slow");
    }

    function resetInput(){
        document.getElementById('form').reset();
    }
</script>