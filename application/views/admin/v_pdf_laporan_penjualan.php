<?php

    //include FCPATH."application/libraries/mpdf/mpdf.php";
    //include FCPATH."application/helpers/rupiah_helper.php";
	
    $pdf=new mPDF();
	
	$margin_top=45;
	$margin_bottom=10;
	$margin_left=10;
	$margin_right=10;
	
	$letter_size	= "A4";
	$pdf=new mPDF('A4',$letter_size,$font_family,$margin_bottom,$margin_left,$margin_right,$margin_top);

    //print_r($dataTransaksi);exit;

    $no=1;
    $dataItem="";
    $totalTagihan=0;
    foreach($dataTransaksi as $row){
        $dataItem.="<tr>
                    <td style='width:10%;font-size:12px;text-align:left'>".$row->nofak_trans."</td>
                    <td style='width:10%;font-size:12px;text-align:center'>".dateIndo(date('Y-m-d', strtotime($row->date_trans)))."</td>
                    <td style='width:10%;font-size:12px;text-align:left'>".$row->karyawan_nama."</td>
                    <td style='width:10%;font-size:12px;text-align:left'>".$row->menu_nama."</td>
                    <td style='width:10%;font-size:12px;text-align:right'>".rupiah($row->harga)."</td>
                    <td style='width:10%;font-size:12px;text-align:center'>".$row->jumlah."</td>
                    <td style='width:10%;font-size:12px;text-align:right'>".rupiah($row->subtotal)."</td>
                </tr>";
        $no++;
        $totalTagihan+=$row->subtotal;
    }

    $logo = FCPATH ."uploads/logo_stallo.png";
    $header="   <table style='width:100%;border-collapse:collapse;' >
                    <tbody>
                        <tr>
                            <td width='20%' rowspan='2' align='center'>
                                <img style='width:auto;height:100px;' src='".$logo."' />
                            </td>    
                            <td width='80%'><h2>Stallo Steak and Spaghetti</h2></td>
                        </tr>
                        <tr>
                            <td style='text-align:justify;font-size:12px;'>
                            Jl. Rs. Fatmawati, Pondok Labu, Jakarta Selatan<br/>
                            ( Samping Rs. Prikasih / Utara Kampus UPN Jakarta ) <br/>
                            Delivery Order : 021-75907058 ( Pondok Labu )
                            </td>
                        </tr>                        
                    </tbody>
                </table>";

    

    $html=" <table style='margin-top:10px;width:100%;border-collapse:collapse;' border='1' cellpadding='8'>
                <thead>
                    <tr>
                        <th style='text-align:left'>No. Trans</th>
                        <th style='text-align:center'>Tanggal</th>
                        <th style='text-align:left'>Kasir</th>
                        <th style='text-align:left'>Menu</th>
                        <th style='text-align:right'>Harga</th>
                        <th style='text-align:center'>Jumlah</th>
                        <th style='text-align:right'>Subtotal</th>
                    </tr>
                </thead>
                <tbody>".$dataItem."</tbody>
                <tfoot>
                    <tr>
                        <td style='font-size:12px;text-align:right' colspan='6'>Total Keseluruhan</td>
                        <td style='font-size:12px;text-align:right'>".rupiah($totalTagihan)."</td>>
                    </tr>
                </tfoot>
            </table>";

    $footer='<table width="100%" style="vertical-align: bottom; font-family: serif; 
                font-size: 8pt; color: #000000;">
                <tr>
                    
                    <td width="33%"><span>Jakarta, '.$now.'</span></td>
                    <td width="33%" align="center" >{PAGENO}</td>                    
                    <td width="33%"><span></td>
                </tr>
            </table>
            ';

    

    $pdf->WriteHTML('table{color:#5b5b5b;}',1);
    $pdf->SetHTMLHeader($header);
    $pdf->WriteHTML($html);
    $pdf->SetHTMLFooter($footer);    
	$pdf->Output("Laporan-Penjualan-".$periode_awal."-".$periode_akhir.".pdf",'D');

?>