<div class="modal-header">
    <h4 class="modal-title" id="defaultModalLabel"><?php echo $title?></h4>
</div>
<div class="modal-body">
    <?php
		if(isset($body)):
		
			$this->load->view($body,$data);
		
		elseif(isset($content)):
			echo $content;
		
		endif;
	 ?>
</div>
<div class="modal-footer">
	<?php if(isset($footer) && $footer):?>
    <button id="save" type="button" class="btn bg-black waves-effect">SAVE</button>
    <?php endif ?>
    <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
</div> 