<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Setting extends CI_Controller {

  	public function __construct(){ 
      	parent::__construct();     
          $this->load->model('M_setting','setting');
          $this->load->model('M_karyawan','karyawan');
          
  	}    

	public function index($id_role=1){
	    $data=array(
	            'body'=>'v_setting_akses',
	            'title'=>'Pengaturan Hak Akses', 
	            'navbar'=>'admin',
	            'index'=>'Pengaturan Hak Akses',
    	        'controller'=>'C_Setting',
				'act'=>'daftar',
				'id_role'=>$id_role,
                "dataRole"=>$this->karyawan->getRole(),
                "dataCategoryPage"=>$this->setting->getCategoryPage(),
			);  
			
		//print_r($data);exit; 
		$this->load->view('template_default',$data);
	}

	public function updateAccessUser(){
		$response = $this->setting->updateAccessUser();
		echo $response;
	}
}
