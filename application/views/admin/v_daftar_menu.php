<div class="card">
    <div class="header bg-green">
        <h2>Form menu</h2>
    </div> 
    <div class="body">
        <form id="form" enctype="multipart/form-data">
            <div class="row clearfix">
                <div class="col-md-6">                    
                    <label for="menu_id">Menu Id</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" id="menu_id" name="menu_id" class="form-control" readonly>
                        </div>
                    </div>

                    <label for="menu_nama">Nama menu</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="text" id="menu_nama" name="menu_nama" class="form-control" placeholder="Nama menu yang akan ditampilkan" required>
                        </div>
                    </div>
                    
                    <label for="kategori_id">Pilih Kategori</label>
                    <div class="form-group">
                        <div class="form-line">
                            <select class="form-control show-tick" data-live-search="true" name="kategori_id" id="kategori_id">
                                <option value="" disabled>Silahkan Pilih Kategori Menu</option>
                                <?php
                                    foreach ($dataKategoriMenu as $kategori) {
                                        echo"<option value='".$kategori->kategori_id."'>".$kategori->kategori_nama."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div>                    
                    <label for="menu_img">Pilih Gambar</label>
                    <div class="form-group">
                        <div class="form-line">
                            <input type="hidden" id="old_image" name="old_image" class="form-control">
                            <input type="file" name="menu_img" id="menu_img" onchange="previewImage('menu_img','prevImg');" />
                        </div>
                    </div>                                        
                    <!-- <label for="prevImg">&nbsp;</label>
                    <img  heigth="100px" width="100px" id="prevImg" src="<?=base_url('uploads/menu/')?>default.png" class="img-thumbnail" alt="image preview" > -->
                    <a id="saveForm" href="javascript:void(0);" class="bg-black btn waves-effect">Simpan</a>
                </div>
                <div class="col-md-6">
                    <div class="col-md-6">
                        <label for="menu_harga">Harga</label>
                        <div class="form-group">
                            <div class="form-line">
                            <input type="text" id="rupiah" name="menu_harga" class="form-control" placeholder="Harga yang akan dijual">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label for="menu_stok">Stok</label>
                        <div class="form-group">
                            <div class="form-line">
                            <input type="number" id="menu_stok" name="menu_stok" class="form-control" placeholder="Perkiraan Jumlah Stok yang ada">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label for="menu_status_aktif">Status Menu</label>
                        <div class="form-group">
                            <div class="demo-switch">
                                <div class="switch">
                                    <label>Non Aktif<input id="menu_status_aktif" name="menu_status_aktif" type="checkbox"><span class="lever switch-col-red"></span>Aktif</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label for="menu_deskripsi">Deksripsi Menu</label>
                        <div class="form-group">
                            <div class="form-line">
                                <textarea id="menu_deskripsi" name="menu_deskripsi" rows="5" class="form-control no-resize" placeholder="Ex: Sirloin Steak dibuat dari daging sapi yang berasal dari sapi bagian belakang tepatnya di atas. daging ini memiliki ke-khasan yang sangat berbeda dari bagian sapi lainya. Kalau dilihat Sirloin Steak memiliki lapisan lemak dibagian atas, yang merupakan ciri khas unik Sirloin Steak ini."></textarea>
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </form>
    </div>
</div>

<div class="card">
    <div class="header bg-green">
        <h2>
            <?=$title?>
        </h2>
    </div>
    <div class="body">
        <div class="table-responsive">
            <table id="tableAjaxmenu" class="table table-bordered table-striped table-hover dataTable js-exportable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nama</th>
                        <th>Harga</th>
                        <th>Gambar</th>
                        <th>Status</th>
                        <th>Stok</th>
                        <th>Kategori</th>
                        <th width="17%">#</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<script>
    function previewImage(id,imagePrev) {
        document.getElementById(imagePrev).style.display = "block";
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById(id).files[0]);
        oFReader.onload = function(oFREvent) {
            document.getElementById(imagePrev).src = oFREvent.target.result;
        };
    };

    $(document).ready(function() {        
        getmenuAll();
    });

    $(document).on("click","#saveForm",function(){
        var menu_nama = $("#menu_nama").val();
        var menu_harga = $("#rupiah").val();
        var menu_stok= $("#menu_stok").val();
        var menu_deskripsi= $("#menu_deskripsi").val();
        
        if(menu_nama==""){
            swal("Informasi","Nama Menu tidak boleh kosong", "info");
            return false;
        }
        if(menu_harga==""){
            swal("Informasi","Harga Menu tidak boleh kosong", "info");
            return false;
        }
        if(menu_stok==""){
            swal("Informasi","Stok Menu tidak boleh kosong", "info");
            return false;
        }
        if(menu_deskripsi==""){
            swal("Informasi","Deskripsi Menu tidak boleh kosong", "info");
            return false;
        }
        
        insertDatamenu();                
    });

    function getmenuId(){
        $.ajax({ 
			url: "<?php echo site_url("admin/c_menu/getmenuId")?>",
			dataType: "json",
			type: "GET",
			success: function(data){
                $("#menu_id").val(data.response.menu_id);
			}
     	});
    }

    function getmenuAll(){        
        $("#tableAjaxmenu").DataTable( {
            "destroy": true,
            "bLengthChange": true,
            "filter": true,
            "dom": 'Bfrtip',
            buttons: [            
                {
                    extend: 'copy',
                    messageTop: 'Copy Data menu',
                    className: 'btn bg-grey waves-effect'
                },
                {
                    extend: 'excel',
                    messageTop: 'Data menu',
                    className: 'btn bg-green waves-effect'
                },
                {
                    extend: 'pdf',
                    messageBottom: null,
                    className: 'btn bg-red waves-effect'
                },
                {
                    extend: 'print',
                    className: 'btn bg-black waves-effect',
                    messageTop: function () {
                        printCounter++;
     
                        if ( printCounter === 1 ) {
                            return 'This is the first time you have printed this document.';
                        }
                        else {
                            return 'You have printed this document '+printCounter+' times';
                        }
                    },
                    messageBottom: null
                }
            ],
            "ajax": {
                url : "<?php echo site_url("admin/c_menu/getmenuAll") ?>",
                type: "GET",
            },
        } );    
        getmenuId();
    }

    function insertDatamenu() {
        //var dataPost = $("form").serialize();
        var data = new FormData(document.getElementById("form")); //harus menggunakan ini untuk upload image
        //dan tambahkan ini pada aajaxnya
        // async: false,
        // processData: false,
        // contentType: false,
        //console.log(dataPost);

        swal({
            title: "Apakah kamu yakin ?",
            text: "Kamu akan menyimpan data ini",
            type: "info",
            showCancelButton: true,
            cancelButtonColor: "#F44336",
            confirmButtonColor: "#4CAF50",
            confirmButtonText: "Yes",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function (isConfirm) {
            if (!isConfirm) return;
            $.ajax({
                url: "<?php echo site_url('admin/c_menu/insertDatamenu')?>",
                type: "POST",
                //async: false,
                processData: false,
                contentType: false,
                data: data,
                success: function (data) {
                    var obj = jQuery.parseJSON(data)
                    //console.log(data);
                    var kode =obj.metaData.kode;
                    var pesan =obj.metaData.message;
                    if(kode=="200"){
                        swal("Berhasil",pesan, "success");                            
                        scrollToBot();
                        getmenuAll();
                        resetInput();                        
                    }else{
                        swal("Simpan Gagal",pesan, "error");
                    }
                    
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error koneksi !", "silahkan coba lagi "+thrownError, "error");
                }
            });
        });
    }

    $(document).on("click","#updatemenu",function(e){
        e.preventDefault();
        var menu_id = $(this).attr('href').replace(/^.*?(#|$)/,'');	
        getmenuBymenuId(menu_id);
    });

    $(document).on("click","#deletemenu",function(e){
        e.preventDefault();
        var menu_id = $(this).attr('href').replace(/^.*?(#|$)/,'');	
        deletemenu(menu_id);
        //getmenuAll();
    });

    function deletemenu(menu_id){
        swal({
            title: "Apakah kamu yakin ?",
            text: "Kamu akan menghapus data ini",
            type: "info",showCancelButton: true,
            cancelButtonColor: "#F44336",
            confirmButtonColor: "#4CAF50",
            confirmButtonText: "Yes",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function (isConfirm) {
            if (!isConfirm) return;
            $.ajax({
                url: "<?php echo site_url("admin/c_menu/deletemenu")?>/"+menu_id,
                dataType: "json",
                type: "GET",
                success: function (data) {
                    var kode = data.response.kode;              
				    var message = data.response.message;
                    if(kode=="200"){
                        swal("Berhasil",message, "success");                            
                        scrollToBot();
                        getmenuAll();
                        resetInput();
                    }else{
                        swal("Hapus data gagal",message, "error");
                    }
                    
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error koneksi !", "silahkan coba lagi "+thrownError, "error");
                }
            });
        });    
    }
    
    function getmenuBymenuId(menu_id){
		
		$.ajax({ 
			url: "<?php echo site_url("admin/c_menu/getmenuBymenuId")?>/"+menu_id,
			dataType: "json",
			type: "GET",
			success: function(data){
                $("#menu_id").val(data.datamenu.menu_id);
                $("#menu_nama").val(data.datamenu.menu_nama);
                $("#rupiah").val(formatRupiah(data.datamenu.menu_harga, "Rp. "));
                $("#old_image").val(data.datamenu.menu_img);                
                $("#menu_deskripsi").val(data.datamenu.menu_deskripsi);
                $("#menu_stok").val(data.datamenu.menu_stok);                
				$("#kategori_id").val(data.datamenu.kategori_id).change();
                
                if(data.datamenu.menu_status_aktif=="aktif"){
                    $("#menu_status_aktif").prop('checked',true);                    
                }else{
                    $("#menu_status_aktif").prop('checked',false);
                }

			  	scrollToTop();
			}
     	});
    }


    
    function scrollToTop(){
        $("html, body").animate({ scrollTop: 20 }, "slow");        
    }
    function scrollToBot(){
        $("html, body").animate({ scrollTop: 1000 }, "slow");
    }

    function resetInput(){
        document.getElementById('form').reset();        
        $("#menu_status_aktif").removeAttr("checked");
        //document.location='<?php echo site_url("admin/c_menu")?>';
    }
</script>