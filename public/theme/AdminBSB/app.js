 
/*
	 * MODAL OPTIONS
	 * 
	 */ 
	
	var sepapp = ",";
	//tooltip
	$("[title]").tooltip({placment:'bottom'});
	
	
	/*
	 * AFTER HIDE
	 */ 
	$(document).on('hidden.bs.modal','.modal',function(){
		$(document.body).removeData();
		$(this).removeData();
	});
	
	/*
	 * BEFORE HIDE
	 */ 
	$(document).on('hide.bs.modal','.modal',function(){
		$(document.body).removeData();
		$(this).removeData();
	});

	/*
	 * AFTER SHOW
	 */ 
	$(document).on('shown.bs.modal','.modal',function(){
		$(this).trigger('after.init.bs.modal');
	});

	/*
	 * BEFORE SHOW
	 */ 
	$(document).on('show.bs.modal','.modal',function(){
		$(this).trigger('after.init.bs.modal');
	});
	
        /*
         * ADD CUSTOM CLASS
         */
        $(document).on('click','[data-target-class]',function(){
            var t = $(this);
            var m = t.data('target') ? $(t.data('target')) : $(t.attr('href'));
            var c = t.attr('data-target-class');
            
            if(typeof m !== 'undefined'){
                m.addClass(c);
                m.on('hidden.bs.modal',function(){
                    $(this).removeClass(c);
                });
            }
        });
        /*
         * SET MODAL TITLE
         */
        
	$(document).on('click','[data-toggle="modal"]',function(e){
	   var t = $(this);
	   var l = t.attr('title');
	   var m = t.data('target') ? $(t.data('target')) : $(t.attr('href'));
	   if( typeof l !== 'undefined' && typeof m !== 'undefined')$('.modal-title',m).text(l);
	});

	
	