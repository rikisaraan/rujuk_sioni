<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Karyawan extends CI_Controller {

  	public function __construct(){ 
      	parent::__construct();     
      	$this->load->model('M_karyawan','karyawan');
  	}    

	public function index(){
	    $data=array(
	            'body'=>'v_daftar_karyawan',
	            'title'=>'Daftar Karyawan', 
	            'navbar'=>'admin',
	            'index'=>'Daftar Karyawan',
    	        'controller'=>'C_Karyawan',
				'act'=>'daftar',
				"dataRole"=>$this->karyawan->getRole(),
			);  
			
		//print_r($data);exit; 
		$this->load->view('template_default',$data);
	}

	public function getKaryawanAll(){
		$this->karyawan->getKaryawanAll();
	}

	public function getKaryawanId(){
		$response = $this->karyawan->createAutoNik();	
		echo $response;
	}

	public function viewProfile(){
		$karyawan_id = $this->session->userdata("karyawan_id");
		$data=array(
	            'body'=>'v_profile_karyawan',
	            'title'=>'Info Karyawan', 
	            'navbar'=>'admin',
	            'index'=>'Info Karyawan',
				'controller'=>'main/dashboard',
				"dataKaryawan"=>$this->karyawan->getKaryawanById($karyawan_id),
    	        'act'=>'daftar',
	        );  
		  $this->load->view('template_default',$data);
	}

	public function getKaryawanByKaryawanId($karyawan_id=""){
		$dataKaryawan = $this->karyawan->getKaryawanByKaryawanId($karyawan_id);
		echo $dataKaryawan;
	}
	
	public function deleteKaryawan($karyawan_id){
		$response = $this->karyawan->deleteKaryawan($karyawan_id);	
		echo $response;
	}
	public function insertDataKaryawan(){
		$response = $this->karyawan->insertDataKaryawan();	
		echo $response;
	}
}
