/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100119
 Source Host           : localhost:3306
 Source Schema         : db_app_resto

 Target Server Type    : MySQL
 Target Server Version : 100119
 File Encoding         : 65001

 Date: 10/08/2019 16:23:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ar_karyawan
-- ----------------------------
DROP TABLE IF EXISTS `ar_karyawan`;
CREATE TABLE `ar_karyawan`  (
  `karyawan_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `karyawan_nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `karyawan_tgl_lahir` date NULL DEFAULT NULL,
  `karyawan_telp` varchar(13) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `karyawan_alamat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `karyawan_tgl_daftar` date NULL DEFAULT NULL,
  `karyawan_photo` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `karyawan_password` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `id_role` int(2) NULL DEFAULT NULL,
  PRIMARY KEY (`karyawan_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ar_karyawan
-- ----------------------------
INSERT INTO `ar_karyawan` VALUES ('K1903210001', 'rizky', '1992-02-01', '0219212122', 'jalan jawa', '2019-07-28', NULL, 'gvqCfRgeQRbe5aYgkRxIfFlSZxRqk4gAYhY6ztqjtAQ7Vefz62Pu+d7BWETvxnJsmi/7xfKlCoj+OTyGfrGp/Q==', 1);
INSERT INTO `ar_karyawan` VALUES ('K1903210002', 'saraan', '1992-02-01', '0219212122', 'jalan jawa', '2019-07-28', NULL, 'ztYxX4vztsevWp6SUPxX4MLQYLlEYsZXNdrD/MSDqJ0c6L5V7845vrZ08jRnVeHWmC0IT6c+Wa7bZqOloMinTw==', 2);
INSERT INTO `ar_karyawan` VALUES ('K1904220003', 'junaidi', '1999-03-16', '111111111111', 'jalan jalan update', '2019-07-28', NULL, 'aLZhhGWhDfDZcvQ06sDLfy1iUzsP5DdC3eTYs4P2TCrnReqsOBzc2qM3GNU3vKugl8f3/kobJNtQd44tSiomjg==', 3);
INSERT INTO `ar_karyawan` VALUES ('K1904230004', 'joko 123', '1990-06-05', '02122222', 'jalan sulawesi', '2019-07-28', NULL, 'R9fAAmtRLXTXncKBbqPET6ofeaI2i8sc7avGbBfvdjQYxlg96dZrMKxT7kT0mdyjjNxDu4rT5JUKmAZvAa6rZw==', 4);
INSERT INTO `ar_karyawan` VALUES ('K1907270005', 'Endah Ratnasari', '1991-02-27', '08977112233', 'Jalan pondok cabe lama', '2019-07-28', NULL, '22zm/85U2GtzZK9YuUKLQ7LibMmpTL4GUPACI1pcyB3LVLStDuGtNznqSePYBmjvlBImPLoaQ94ZuOjARpvMCA==', 5);

-- ----------------------------
-- Table structure for ar_kategori_menu
-- ----------------------------
DROP TABLE IF EXISTS `ar_kategori_menu`;
CREATE TABLE `ar_kategori_menu`  (
  `kategori_id` int(2) NOT NULL AUTO_INCREMENT,
  `kategori_nama` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kategori_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ar_kategori_menu
-- ----------------------------
INSERT INTO `ar_kategori_menu` VALUES (1, 'Minuman');
INSERT INTO `ar_kategori_menu` VALUES (2, 'Makanan');

-- ----------------------------
-- Table structure for ar_meja
-- ----------------------------
DROP TABLE IF EXISTS `ar_meja`;
CREATE TABLE `ar_meja`  (
  `id_meja` int(2) NOT NULL AUTO_INCREMENT,
  `nama_meja` varchar(7) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi_meja` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_meja`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ar_meja
-- ----------------------------
INSERT INTO `ar_meja` VALUES (1, 'Meja 01', 'Meja dengan 2 kursi duduk');
INSERT INTO `ar_meja` VALUES (2, 'Meja 02', 'Meja dengan kursi lesehan kapasitas maksimal 4 orang');
INSERT INTO `ar_meja` VALUES (3, 'Meja 03', 'Meja dengan kursi lesehan kapasitas maksimal 4 orang');
INSERT INTO `ar_meja` VALUES (4, 'Meja 04', 'Meja dengan kursi lesehan kapasitas maksimal 4 orang');
INSERT INTO `ar_meja` VALUES (5, 'Meja 05', 'Meja dengan kursi lesehan kapasitas maksimal 4 orang');
INSERT INTO `ar_meja` VALUES (6, 'Meja 06', 'Meja dengan kursi lesehan kapasitas maksimal 4 orang');
INSERT INTO `ar_meja` VALUES (7, 'Meja 07', 'Meja dengan kursi lesehan kapasitas maksimal 4 orang');
INSERT INTO `ar_meja` VALUES (8, 'Meja 08', 'Meja dengan kursi lesehan kapasitas maksimal 4 orang');
INSERT INTO `ar_meja` VALUES (9, 'Meja 09', 'Meja dengan kursi lesehan kapasitas maksimal 4 orang');
INSERT INTO `ar_meja` VALUES (10, 'Meja 10', 'Meja dengan kursi lesehan kapasitas maksimal 4 orang');

-- ----------------------------
-- Table structure for ar_menu
-- ----------------------------
DROP TABLE IF EXISTS `ar_menu`;
CREATE TABLE `ar_menu`  (
  `menu_id` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `menu_nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_deskripsi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `menu_tgl_dibuat` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `menu_stok` int(2) NULL DEFAULT NULL,
  `menu_harga` double NULL DEFAULT NULL,
  `menu_img` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'default.jpg',
  `menu_status_aktif` enum('aktif','non-aktif') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kategori_id` int(2) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ar_menu
-- ----------------------------
INSERT INTO `ar_menu` VALUES ('M0001', 'Sirloin', 'Sirloin Steak dibuat dari daging sapi yang berasal dari sapi bagian belakang tepatnya di atas. daging ini memiliki ke-khasan yang sangat berbeda dari bagian sapi lainya. Kalau dilihat Sirloin Steak memiliki lapisan lemak dibagian atas, yang merupakan ciri khas unik Sirloin Steak ini.', '2019-04-23 22:56:34', 9, 20000, 'M0001.jpg', 'aktif', 2);
INSERT INTO `ar_menu` VALUES ('M0002', 'Jus Alpukat', 'Alpukat yang diblender dengan campuran susu dan Es batu', '2019-04-23 23:02:47', 39, 10000, 'M0002.jpg', 'aktif', 1);
INSERT INTO `ar_menu` VALUES ('M0003', 'Chicken Mushroom', 'chicken steak dicampur dengan saus jamur yang begitu menggoda', '2019-07-16 23:55:34', 80, 22000, 'M0003.jpg', 'aktif', 2);
INSERT INTO `ar_menu` VALUES ('M0004', 'Jus Apel', 'Apel yang diblender dengan dicampuri pemanis yang membuat jus semakin nikmat', '2019-07-24 16:14:18', 47, 8000, 'M0004.jpg', 'aktif', 1);
INSERT INTO `ar_menu` VALUES ('M0005', 'Jus Melon', 'Melon yang diblender dengan campuran air gula ', '2019-07-24 16:14:58', 43, 8000, 'M0005.jpg', 'aktif', 1);

-- ----------------------------
-- Table structure for ar_page
-- ----------------------------
DROP TABLE IF EXISTS `ar_page`;
CREATE TABLE `ar_page`  (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `page_label` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `page_icon` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `category_id` int(2) NOT NULL,
  `page_parent_id` int(2) NOT NULL,
  `page_active` int(2) NULL DEFAULT 1,
  `page_target_blank` int(2) NULL DEFAULT 0,
  `page_sort` int(2) NULL DEFAULT NULL,
  PRIMARY KEY (`page_id`) USING BTREE,
  UNIQUE INDEX `a`(`page_id`) USING BTREE,
  INDEX `b`(`page_name`, `page_label`, `page_icon`, `category_id`, `page_parent_id`, `page_active`, `page_target_blank`, `page_sort`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ar_page
-- ----------------------------
INSERT INTO `ar_page` VALUES (1, 'admin/C_Karyawan', 'Data Karyawan', 'person_pin', 1, 0, 1, 0, 1);
INSERT INTO `ar_page` VALUES (4, 'admin/C_Meja', 'Data Meja', 'camera_front', 1, 0, 1, 0, 3);
INSERT INTO `ar_page` VALUES (2, 'admin/C_Menu', 'Data Menu', 'content_paste', 1, 0, 1, 0, 2);
INSERT INTO `ar_page` VALUES (6, 'admin/C_Report', 'Laporan Penjualan', 'assessment', 4, 0, 1, 0, 1);
INSERT INTO `ar_page` VALUES (3, 'admin/C_Setting', 'Hak Akses', 'lock_outline', 2, 0, 1, 0, 2);
INSERT INTO `ar_page` VALUES (5, 'admin/C_Tagihan/bayarTagihan', 'Pembayaran Tagihan', 'account_balance_wallet', 3, 0, 1, 0, 1);
INSERT INTO `ar_page` VALUES (7, 'admin/C_Transaction/listAntrian', 'List Antrian Pesanan', 'shopping_basket', 3, 0, 1, 0, 2);

-- ----------------------------
-- Table structure for ar_page_category
-- ----------------------------
DROP TABLE IF EXISTS `ar_page_category`;
CREATE TABLE `ar_page_category`  (
  `category_id` int(2) NOT NULL AUTO_INCREMENT,
  `category_title` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `category_icon` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `category_sort` int(2) NULL DEFAULT NULL,
  PRIMARY KEY (`category_id`) USING BTREE,
  UNIQUE INDEX `a`(`category_id`) USING BTREE,
  INDEX `b`(`category_title`) USING BTREE,
  INDEX `c`(`category_icon`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ar_page_category
-- ----------------------------
INSERT INTO `ar_page_category` VALUES (1, 'Data Master', 'list', 1);
INSERT INTO `ar_page_category` VALUES (2, 'Pengaturan', 'settings', 4);
INSERT INTO `ar_page_category` VALUES (3, 'Transaksi', 'add_shopping_cart', 2);
INSERT INTO `ar_page_category` VALUES (4, 'Laporan', 'assignment', 3);

-- ----------------------------
-- Table structure for ar_role
-- ----------------------------
DROP TABLE IF EXISTS `ar_role`;
CREATE TABLE `ar_role`  (
  `id_role` int(2) NOT NULL AUTO_INCREMENT,
  `nama_role` varchar(7) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_role`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ar_role
-- ----------------------------
INSERT INTO `ar_role` VALUES (1, 'Admin');
INSERT INTO `ar_role` VALUES (2, 'Kasir');
INSERT INTO `ar_role` VALUES (3, 'Koki');
INSERT INTO `ar_role` VALUES (4, 'Pelayan');
INSERT INTO `ar_role` VALUES (5, 'Owner');

-- ----------------------------
-- Table structure for ar_trans_detail
-- ----------------------------
DROP TABLE IF EXISTS `ar_trans_detail`;
CREATE TABLE `ar_trans_detail`  (
  `harga` double NULL DEFAULT NULL,
  `jumlah` int(2) NULL DEFAULT NULL,
  `detail_status` enum('order','delivery') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'order',
  `subtotal` double NULL DEFAULT NULL,
  `nofak_trans` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `menu_id` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  INDEX `menu_id`(`menu_id`) USING BTREE,
  INDEX `nofak_trans`(`nofak_trans`) USING BTREE,
  CONSTRAINT `menu_id` FOREIGN KEY (`menu_id`) REFERENCES `ar_menu` (`menu_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `nofak_trans` FOREIGN KEY (`nofak_trans`) REFERENCES `ar_transaction` (`nofak_trans`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ar_trans_detail
-- ----------------------------
INSERT INTO `ar_trans_detail` VALUES (10000, 2, 'order', 20000, '1908060001', 'M0002');
INSERT INTO `ar_trans_detail` VALUES (8000, 2, 'order', 16000, '1908060002', 'M0005');
INSERT INTO `ar_trans_detail` VALUES (8000, 2, 'order', 16000, '1908060003', 'M0005');
INSERT INTO `ar_trans_detail` VALUES (10000, 2, 'order', 20000, '1908060004', 'M0002');
INSERT INTO `ar_trans_detail` VALUES (20000, 1, 'order', 20000, '1908060004', 'M0001');
INSERT INTO `ar_trans_detail` VALUES (22000, 1, 'order', 22000, '1908060003', 'M0003');
INSERT INTO `ar_trans_detail` VALUES (8000, 1, 'order', 8000, '1908060004', 'M0005');
INSERT INTO `ar_trans_detail` VALUES (8000, 1, 'order', 8000, '1908060003', 'M0004');
INSERT INTO `ar_trans_detail` VALUES (22000, 2, 'order', 44000, '1908060006', 'M0003');
INSERT INTO `ar_trans_detail` VALUES (20000, 2, 'order', 40000, '1908060005', 'M0001');
INSERT INTO `ar_trans_detail` VALUES (8000, 2, 'order', 16000, '1908060006', 'M0004');
INSERT INTO `ar_trans_detail` VALUES (10000, 2, 'order', 20000, '1908060005', 'M0002');
INSERT INTO `ar_trans_detail` VALUES (10000, 2, 'order', 20000, '1908060008', 'M0002');
INSERT INTO `ar_trans_detail` VALUES (22000, 1, 'order', 22000, '1908060007', 'M0003');

-- ----------------------------
-- Table structure for ar_transaction
-- ----------------------------
DROP TABLE IF EXISTS `ar_transaction`;
CREATE TABLE `ar_transaction`  (
  `nofak_trans` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status_trans` enum('order','ready','complete') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `date_trans` datetime(0) NULL DEFAULT NULL,
  `total_trans` double NULL DEFAULT NULL,
  `bayar_trans` double NULL DEFAULT NULL,
  `kembali_trans` double NULL DEFAULT NULL,
  `id_meja` int(2) NULL DEFAULT NULL,
  `karyawan_id` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `petugas_kasir` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`nofak_trans`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ar_transaction
-- ----------------------------
INSERT INTO `ar_transaction` VALUES ('1908060001', 'complete', '2019-08-06 15:06:13', 20000, NULL, NULL, 2, 'K1903210001', NULL);
INSERT INTO `ar_transaction` VALUES ('1908060002', 'complete', '2019-08-06 15:06:13', 16000, NULL, NULL, 1, 'K1903210001', NULL);
INSERT INTO `ar_transaction` VALUES ('1908060003', 'complete', '2019-08-06 15:07:36', 46000, NULL, NULL, 1, 'K1903210001', NULL);
INSERT INTO `ar_transaction` VALUES ('1908060004', 'complete', '2019-08-06 15:07:36', 48000, NULL, NULL, 2, 'K1903210001', NULL);
INSERT INTO `ar_transaction` VALUES ('1908060005', 'complete', '2019-08-06 15:10:51', 60000, NULL, NULL, 1, 'K1903210001', NULL);
INSERT INTO `ar_transaction` VALUES ('1908060006', 'complete', '2019-08-06 15:10:51', 60000, NULL, NULL, 2, 'K1903210001', NULL);
INSERT INTO `ar_transaction` VALUES ('1908060007', 'order', '2019-08-06 15:12:00', 22000, NULL, NULL, 1, 'K1903210001', NULL);
INSERT INTO `ar_transaction` VALUES ('1908060008', 'order', '2019-08-06 15:12:00', 20000, NULL, NULL, 4, 'K1903210001', NULL);

-- ----------------------------
-- Table structure for ar_user_role
-- ----------------------------
DROP TABLE IF EXISTS `ar_user_role`;
CREATE TABLE `ar_user_role`  (
  `id_user_role` int(15) NOT NULL AUTO_INCREMENT,
  `page_id` int(15) NULL DEFAULT NULL,
  `id_role` int(15) NULL DEFAULT NULL,
  PRIMARY KEY (`id_user_role`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 69 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ar_user_role
-- ----------------------------
INSERT INTO `ar_user_role` VALUES (59, 1, 1);
INSERT INTO `ar_user_role` VALUES (60, 2, 1);
INSERT INTO `ar_user_role` VALUES (61, 4, 1);
INSERT INTO `ar_user_role` VALUES (62, 3, 1);
INSERT INTO `ar_user_role` VALUES (63, 5, 1);
INSERT INTO `ar_user_role` VALUES (64, 7, 1);
INSERT INTO `ar_user_role` VALUES (65, 6, 1);
INSERT INTO `ar_user_role` VALUES (66, 7, 3);
INSERT INTO `ar_user_role` VALUES (67, 5, 2);
INSERT INTO `ar_user_role` VALUES (68, 6, 5);

SET FOREIGN_KEY_CHECKS = 1;
