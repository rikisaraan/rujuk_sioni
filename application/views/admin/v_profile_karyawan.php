<div class="row clearfix">
    <div class="col-xs-12 col-sm-3">
        <div class="card profile-card">
            <div class="profile-header">&nbsp;</div>
            <div class="profile-body">
                <div class="image-area">
                    <img src="<?=base_url("public/theme/AdminBSB/")?>images/user-lg.png" alt="AdminBSB - Profile Image">
                </div>
                <div class="content-area">
                    <h3><?=$dataKaryawan->karyawan_nama?></h3>
                    <p><?=$dataKaryawan->nama_role?></p>
                </div>
            </div>          
        </div>

        <div class="card card-about-me">
            <div class="header">
                <h2>Tentang Saya</h2>
            </div>
            <div class="body">
                <ul>
                    <li>
                        <div class="title"><i class="material-icons">location_on</i>Alamat</div>
                        <div class="content"><?=$dataKaryawan->karyawan_alamat?></div>
                    </li>
                    <li>
                        <div class="title"><i class="material-icons">phone</i>Telepon</div>
                        <div class="content"><?=$dataKaryawan->karyawan_telp?></div>
                    </li>
                    <li>
                        <div class="title"><i class="material-icons">today</i>Tanggal Lahir</div>
                        <div class="content"><?=$dataKaryawan->karyawan_tgl_lahir?></div>
                    </li>
                    <li>
                        <div class="title"><i class="material-icons">today</i>Tanggal Terdaftar</div>
                        <div class="content"><?=$dataKaryawan->karyawan_tgl_daftar?></div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="card">
            <div class="header">
                <h2>History Log Aktifitas Karyawan</h2>                
            </div>
            <div class="body">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header bg-green"><h2>Log Aktifitas</h2></div>
                            <div class="body">isi log</div>
                        </div>
                    </div>                
                </div>
            </div>
        </div>
    </div>
</div>